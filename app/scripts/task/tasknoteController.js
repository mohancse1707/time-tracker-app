/**
 * Created by Mohan on 2/24/2016.
 */

// angular.module('timeTracker', []).config(function($stateProvider){

//   $stateProvider.state()
// })


angular.module('todoControllers', [])
.controller('TaskTrackerCtrl', ['$scope', '$routeParams',
  function($scope, $routeParams){

    $scope.tasks = db('todo').cloneDeep();
    $scope.tasks = [
    {
      "name": "Task 1",
      "description": "Test1",
      "done": true
    },
    {
      "name": "Write a simple tutorial",
      "description": "Write a tutorial about Angular.js + Node.js",
      "done": true
    },
    {
      "name": "Hello!",
      "description": "Simple",
      "done": false
    },
    {
      "name": "Ok",
      "description": "asdas",
      "done": false
    }
    ];

    $scope.my_task = null;

    $scope.currentDate = new Date();

    $scope.removeTask = function(task){
      var query = {name: task.name, description: task.description, done: task.done};
      db('todo').remove(query);
      $scope.tasks = db('todo').cloneDeep();
    };

    $scope.viewTask = function(task){
      $scope.my_task = task;
      $("#modalView").modal();
    };
  }
])
.controller('AddTaskCtrl', ['$scope', '$routeParams', '$location',
  function($scope, $routeParams, $location){

 $scope.tasks = [
    {
      "name": "Task 1",
      "description": "Test1",
      "done": true
    },
    {
      "name": "Write a simple tutorial",
      "description": "Write a tutorial about Angular.js + Node.js",
      "done": true
    },
    {
      "name": "Hello!",
      "description": "Simple",
      "done": false
    },
    {
      "name": "Ok",
      "description": "asdas",
      "done": false
    }
    ];

$scope.saveTask = function(task)
{

}

    $scope.newTask = function(){
      var t = $scope.task;
      var data = {name: t.name, description: t.description, done: t.done};
      $scope.tasks.push(data);
      $location.path("/");
    };
  }
]);
