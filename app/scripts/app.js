"use strict";

// var todoApp = angular.module('timeTracker', [
//   'ngRoute',
//   'todoControllers'
// ]);

// var low = require('lowdb');
// var db = low('db.json');

// todoApp.config(['$routeProvider',

// function($routeProvider) {
//   $routeProvider.
//     when('/', {
//       templateUrl: 'views/task/tasknote.html',
//       controller: 'TaskTrackerCtrl'
//     }).
//     when('/todo', {
//       templateUrl: 'views/task/addtask.html',
//       controller: 'AddTaskCtrl'
//     }).
//     otherwise({
//       redirectTo: '/'
//     });
// }]);


angular.module('timeTracker', [])
  .run(function ($rootScope, $timeout) {
    $rootScope.NW = require('nw.gui');
    $rootScope.tray = new $rootScope.NW.Tray({title: 'Tray', icon: 'flat-avatar.png'});
    var nw = $rootScope.NW;
    var win = nw.Window.get();
    var tray = $rootScope.tray;
    var menu = new nw.Menu({type: "menubar"});
    $scope.currentDate = new Date("2011-04-20 09:30:51.01");
    if (process.platform == "darwin") {
      menu.createMacBuiltin && menu.createMacBuiltin('Time Tracker');
    }
    var submenu = new nw.Menu();
    submenu.append(new nw.MenuItem({
      label: 'About',
      click: function() {
        nw.Window.open('/task/tasknote.html');

      }
    }));
    // Create and append the 1st level menu to the menubar
    menu.append(new nw.MenuItem({
      label: 'Help',
      submenu: submenu
    }));

    win.menu = menu;

    win.on('minimize', function() {
      this.hide();
    });

    tray.on('click',function(){
      win.show();
    });




  });
